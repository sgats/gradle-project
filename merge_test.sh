gitlab_uname=zhaofuxin
gitlab_passwd=zhaofuxin
#打包环境
dock_test="10.10.20.228"
dock_pre="172.16.2.26"
dock_online="10.9.0.14 192.168.9.60"
dock_android="10.10.10.54"
s_branch=$1
t_branch=$2	
if_tag=$(echo $3|tr [A-Z] [a-z])
tag=tag
tag_name=${t_branch}_$(date +%Y%m%d%H%M)
#设置git 环境
ip=$(hostname -I|sed -e 's/^[ \t]*//g' -e 's/[ \t]*$//g')
git="/usr/bin/git"
#设置邮箱
$git config --local user.email "${gitlab_uname}@jyall.com"
$git config --local user.name "${gitlab_uname}"

#设置用户名密码
num=`$git remote -v | awk '{print $2}'|sort|  uniq |wc -l`
if [ $num -eq 1 ];then
	remote_url=`$git remote -v | awk '{print $2}' |sort | uniq `
	echo $remote_url| grep "https://$gitlab_uname:$gitlab_passwd@gitlab.jyall.com"	> /dev/null
	if [ $? -ne 0 ];then
		new_remote_url=`echo $remote_url| sed 's/\(gitlab\.jyall\.com\)/'$gitlab_uname:$gitlab_passwd'@\1/g'`		
		$git remote set-url origin $new_remote_url
	fi
fi
#切换分支 
$git branch | grep $t_branch > /dev/null
if [ $? -eq 0 ];then

	$git checkout -f $t_branch
else
	$git checkout -b $t_branch origin/$t_branch
fi
#更新代码
$git pull origin
#打标签
if [ "$if_tag"x = "$tag"x ];then
	$git tag -a $tag_name -m "Auto make tag for master by jenkins"
fi
# 提交更新
$git merge -Xignore-space-change --no-ff -m "Merge branch '$s_branch' into '$t_branch' at `date +%H:%M:%S` time" origin/$s_branch
if [ $? -eq 0 ];then
	echo "***merge result***"
	$git status
	$git status | grep -E "Your branch is ahead of 'origin/$t_branch' by [0-9]* commits." > /dev/null
	result[0]=$?
	$git status | grep -E "您的分支领先 'origin/$t_branch' 共 [0-9]* 个提交" > /dev/null
	result[1]=$?
	if [ ${result[0]} -eq 0 ] || [ ${result[1]} -eq 0 ];then
		if [ "$if_tag"x = "$tag"x ];then
			$git push origin $tag_name && \
			echo -e "\e[1;32mpush tag $tag_name success!!!\e[0m" || \
			exit 1
		fi
		$git push origin $t_branch  && \
		echo -e "\e[1;32mpush new modify to $t_branch success!!!\e[0m" || \
		exit 1
	else
		echo -e "\e[1;34mno need push file or modify content!!!\e[0m"
		
	fi
else
	echo -e "\e[1;31mmerge branch failed, please resolve this confliced!!!\e[0m"
	$git status
	if [ "$if_tag"x = "$tag"x ];then
		$git tag -d $tag_name
	fi
	$git merge --abort
	exit 
fi


20号  